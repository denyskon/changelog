// Copyright 2020 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package service

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestCleanTitle(t *testing.T) {
	tt := []struct {
		Title    string
		Expected string
	}{
		{Title: "foo", Expected: "Foo"},
		{Title: " foo", Expected: "Foo"},
		{Title: "foo bar", Expected: "Foo bar"},
		{Title: "Foo bar", Expected: "Foo bar"},
		{Title: " Foo bar ", Expected: "Foo bar"},
		{Title: "1234", Expected: "1234"},
	}

	for _, tc := range tt {
		t.Run(tc.Title, func(t *testing.T) {
			s := CleanTitle(tc.Title)
			if s != tc.Expected {
				t.Logf("got %q | expected %q", s, tc.Expected)
				t.Fail()
			}
		})
	}
}
